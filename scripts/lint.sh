#!/bin/sh
# Time-stamp: <2023-12-04 Mon 09:15>
# Copyright (C) 2023 by Morgan Smith

top_srcdir=$(dirname "$0")/..

c_source=$(find "$top_srcdir" -name "*.[ch]")
scm_source=$(find "$top_srcdir" -name "*.scm")

# eliminate trailing whitespace
sed --in-place 's/[[:space:]]\+$//' $c_source $scm_source

# replace tabs with 2 spaces
sed --in-place 's/\t/  /' $c_source $scm_source

# run indent
indent --no-tabs --indent-level2 $c_source
